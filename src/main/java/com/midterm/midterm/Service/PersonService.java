package com.midterm.midterm.Service;

@Service
public class PersonService {
    private final PersonRepository caseRepository;

    public PersonService(PersonRepository PersonRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll() {
        return (List<Person>) personRepository.findAll();
    }


    public Person findById(long id) {
        return personRepository.findById(id);
    }

    public void deleteById(long id) {
        personRepository.deleteById(id);
    }

    public void updateByID(long id, String personHeadingEN) {
        PersonRepository.updatePersonHeadingEnByID(personHeadingEN, id);
    }
}