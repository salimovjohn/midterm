package com.midterm.midterm.Controller;


@RestController
public class PersonController {
    private final PersonService PersonService;
    public PersonController(PersonService personService) {
        this.personService = personService;
    }
    @GetMapping("api/person/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(personService.getById(id));
    }
    @GetMapping("api/person")
    public ResponseEntity<?> findAll(){
        return  ResponseEntity.ok(personService.getAll());
    }
    @DeleteMapping("api/person/{id}")
    public void deleteById(@PathVariable long id){
        personService.deleteById(id);
    }
    @PostMapping("api/person")
    public ResponseEntity<?> createPerson(@RequestBody Authorization authorization){
        return ResponseEntity.ok(personService.createPerson(authorization));
    }




