package com.midterm.midterm.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String city;
    private String phone;
    private String telegram;


}
